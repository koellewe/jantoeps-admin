package com.sps.janadmin;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.DisplayMetrics;
import android.widget.Toast;

import java.io.BufferedInputStream;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;

/**
 * Created by Jans Rautenbach on 2015/09/14.
 */

public class JanTools extends Activity {

    static public void setContext(Context context){
        ctx = context;
    }

    static private Context ctx;

    static public void makeToast(final String s){
        ((Activity) ctx).runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (s.length() < 2){
                    Toast.makeText(ctx, s, Toast.LENGTH_SHORT).show();
                }else if (s.substring(s.length() - 2).equals("/l")){
                    Toast.makeText(ctx, s.substring(0, s.length() - 2), Toast.LENGTH_LONG).show();
                }else{
                    Toast.makeText(ctx, s, Toast.LENGTH_SHORT).show();
                }
            }
        });

    }

    static public int indexOfStrArr(String[] array, String search){

        int pos = 0;
        while (!array[pos].equals(search) && pos < array.length){
            pos++;
        }
        if (pos >= array.length){
            return -1;
        }else{
            return pos;
        }

    }

    static public String readDB(String[] vals, String[] keys, String key){
        return vals[indexOfStrArr(keys, key)];
    }

    public void gotoActivity(Class where){
        Intent intent = new Intent(ctx, where);
        ctx.startActivity(intent);
        finish();
    }

    static public void write(String key, String val){
        SharedPreferences sharedPref = ctx.getSharedPreferences("janadmin.info", MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putString(key, val);
        editor.apply();
    }

    static public String read(String key) {
        SharedPreferences sharedPref = ctx.getSharedPreferences("janadmin.info", MODE_PRIVATE);
        return sharedPref.getString(key, "0");
    }

    static public boolean readCmp(String in, String cmp){
        return read(in).equals(cmp);
    }

    static public boolean readBool(String in){
        return read(in).equals("true");
    }

    static public int countLines(String filename){
        if (!(new File(filename)).exists()){
            return -1;

        }
        try {
            InputStream is = new BufferedInputStream(new FileInputStream(filename));
            try {
                byte[] c = new byte[1024];
                int count = 0;
                int readChars = 0;
                boolean empty = true;
                while ((readChars = is.read(c)) != -1) {
                    empty = false;
                    for (int i = 0; i < readChars; ++i) {
                        if (c[i] == '\n') {
                            ++count;
                        }
                    }
                }
                return (count == 0 && !empty) ? 1 : count + 1;
            } finally {
                is.close();
            }
        }catch (Exception e) {
            System.out.println("Error: " + e);
        }
        return -1;
    }

    public static void printArray(Object[] print){
        for (int i = 0; i < print.length; i++){
            System.out.println(("pos " + i + ": " + print[i]));
        }
    }

    static public boolean checkConnection(){
        ConnectivityManager cm = (ConnectivityManager) ctx.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();

        return activeNetwork != null && activeNetwork.isConnectedOrConnecting();
    }

    static public boolean checkIfWifi(){
        ConnectivityManager cm = (ConnectivityManager) ctx.getSystemService(Context.CONNECTIVITY_SERVICE);
        return cm.getActiveNetworkInfo().getType() == ConnectivityManager.TYPE_WIFI;
    }

    public static float convertPixelsToDp(float px){
        Resources resources = ctx.getResources();
        DisplayMetrics metrics = resources.getDisplayMetrics();
        return px / (metrics.densityDpi / 160f);
    }

    public static void writeLine(String line, BufferedWriter writer) throws Exception{
        writer.write(line);
        writer.newLine();
        writer.flush();
    }

    public static void writeLines(String[] lines, BufferedWriter writer) throws Exception{
        for (String aLine : lines){
            writeLine(aLine, writer);
        }
    }

}


