package com.sps.janadmin;

import android.app.ActionBar;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.InputType;
import android.text.TextDirectionHeuristic;
import android.text.method.ScrollingMovementMethod;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;

public class ViewAndDel extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_and_del);
        JanTools.setContext(ViewAndDel.this);
        ((TextView) findViewById(R.id.txtOutput)).setMovementMethod(new ScrollingMovementMethod());
        new GetUpdate().execute();
    }

    private static final String url_get_afk = "http://apps.rauten.co.za/jantoeps/get_all_afkondigings.php";
    private static final String url_run = "http://apps.rauten.co.za/jantoeps/run.php";

    public void onBackClick(View view) {
        gotoActivity(Afkondigings.class);
    }

    public void gotoActivity(Class where) {
        Intent intent = new Intent(this, where);
        startActivity(intent);
        finish();
    }

    public void onDelClick(View view) {
        AlertDialog.Builder d = new AlertDialog.Builder(this);

        final EditText edt = new EditText(this);
        edt.setInputType(InputType.TYPE_CLASS_NUMBER);
        //edt.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT));

        d.setView(edt);
        d.setMessage("Enter id:");
        d.setCancelable(true);
        d.setPositiveButton("Delete", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                new DelAfk().execute(edt.getText().toString());
            }
        });
        d.setNegativeButton("Cancel", null);
        d.show();

    }

    ProgressDialog dialog;
    class DelAfk extends AsyncTask<String, Void, String>{

        @Override
        public void onPreExecute(){
            super.onPreExecute();
            dialog = new ProgressDialog(ViewAndDel.this);
            dialog.setMessage("Deleting afkondiging...");
            dialog.setIndeterminate(true);//spinning thing
            dialog.setCancelable(false);
            dialog.show();
        }

        @Override
        public String doInBackground(String... args){
            JSONObject json = new JSONObject();

            try {

                json.put("query", "DELETE FROM afkondigings WHERE id=" + args[0]);
                System.out.println("SENT: " + json.toString());

                HttpURLConnection httpCon = (HttpURLConnection) new URL(url_run).openConnection();
                httpCon.setRequestMethod("POST");
                httpCon.setConnectTimeout(10_000);
                httpCon.setReadTimeout(10_000);
                httpCon.setRequestProperty("Content-Type", "application/json");
                httpCon.setRequestProperty("Accept", "application/json");
                httpCon.setDoOutput(true);

                OutputStreamWriter writer = new OutputStreamWriter(httpCon.getOutputStream());
                writer.write(json.toString());
                writer.flush();
                writer.close();

                int result = httpCon.getResponseCode();

                StringBuilder sb = new StringBuilder();
                if (result == HttpURLConnection.HTTP_OK) {
                    BufferedReader br = new BufferedReader(new InputStreamReader(
                            httpCon.getInputStream(), "utf-8"));

                    String line;
                    while ((line = br.readLine()) != null) {
                        sb.append(line);
                        sb.append("\n");
                    }
                    br.close();

                    String output = sb.toString();
                    System.out.print("RESULT: " + output);
                    return (new JSONObject(output)).getString("message");

                } else {
                    System.out.print("FAILED: "+ httpCon.getResponseMessage());
                }

            } catch (JSONException |IOException e) {
                e.printStackTrace();
            }

            return null;
        }

        @Override
        public void onPostExecute(String s){
            dialog.dismiss();

            JanTools.makeToast(s);

            new GetUpdate().execute();//refresh

        }

    }

    class GetUpdate extends AsyncTask<Void, Void, JSONObject> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            dialog = new ProgressDialog(ViewAndDel.this);
            dialog.setMessage("Getting updates...");
            dialog.setIndeterminate(true);//spinning thing
            dialog.setCancelable(false);
            dialog.show();
        }

        @Override
        protected JSONObject doInBackground(Void... v) {

            try {

                HttpURLConnection httpCon = (HttpURLConnection) new URL(url_get_afk).openConnection();
                httpCon.setRequestMethod("POST");
                httpCon.setConnectTimeout(10_000);
                httpCon.setReadTimeout(10_000);
                httpCon.setRequestProperty("Content-Type", "application/json");
                httpCon.setRequestProperty("Accept", "application/json");
                httpCon.setDoOutput(true);

                int result = httpCon.getResponseCode();

                StringBuilder sb = new StringBuilder();
                if (result == HttpURLConnection.HTTP_OK) {
                    BufferedReader br = new BufferedReader(new InputStreamReader(
                            httpCon.getInputStream(), "utf-8"));

                    String line;
                    while ((line = br.readLine()) != null) {
                        sb.append(line);
                        sb.append("\n");
                    }
                    br.close();

                    //System.out.println("RESPONSE = " + sb.toString());

                    return new JSONObject(sb.toString());

                } else {
                    JanTools.makeToast("HTTP Error");
                }

            } catch (JSONException|IOException e) {
                e.printStackTrace();
            }

            return null;

        }

        @Override
        protected void onPostExecute(final JSONObject json) {
            //super.onPostExecute(s);
            dialog.dismiss();

            if (json == null){
                JanTools.makeToast("Error getting afks");
            }else{
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        postCustom(json);
                    }
                });
            }
        }

        private void postCustom(JSONObject json){
            StringBuilder out = new StringBuilder();

            try {
                JSONArray jarr = json.getJSONArray("afkondigings");
                for (int i = 0; i < jarr.length(); i++){
                    JSONObject j_it = ((JSONObject) jarr.get(i));
                    out.append("id: ").append(j_it.getString("id"))
                            .append("\nreg: ").append(j_it.getString("reg_date"))
                            .append("\nexp: ").append(j_it.getString("expiry_date"))
                            .append("\nafk: ").append(j_it.getString("afkondiging"))
                            .append("\n=====\n");
                }
                ((TextView) findViewById(R.id.txtOutput)).setText(out.toString());

            } catch (JSONException e) {
                e.printStackTrace();
                JanTools.makeToast("Error decoding response");
            }
        }
    }
}
